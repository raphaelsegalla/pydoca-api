from models import Produto, Usuario, Venda, ProdutoVenda
from datetime import date, datetime

SQL_DELETA_PRODUTO = 'delete from produto where id = %s'
SQL_PRODUTO_POR_ID = 'SELECT id, nome, valor, categoria from produto where id = %s'
SQL_USUARIO_POR_ID = 'SELECT id, nome, senha from usuario where id = %s'
SQL_ATUALIZA_PRODUTO = 'UPDATE produto SET nome=%s, valor=%s, categoria=%s where id = %s'
SQL_ATUALIZA_PRODUTO_VENDIDO = 'UPDATE produto SET vendido=%s where id = %s'
SQL_BUSCA_PRODUTOS = 'SELECT id, nome, valor, categoria from produto where vendido = true'
SQL_BUSCA_PRODUTOS_VENDIDOS = 'SELECT p.id, p.nome, p.valor, p.categoria, v.data_venda, v.hora_venda from produto p, venda v, produtosvenda pv where p.vendido = false and p.id = pv.id_produto and pv.id_venda = v.id' 
SQL_CRIA_PRODUTO = 'INSERT into produto (nome, valor, categoria, vendido) values (%s, %s, %s, %s)'
SQL_CRIA_VENDA = 'INSERT into venda (valor_venda, data_venda, hora_venda) values (%s, %s, %s)'
SQL_CRIA_PRODUTOS_VENDA = 'INSERT into produtosvenda (id_venda, id_produto) values (%s, %s)'

global lista_produtos_venda
lista_produtos_venda = []

global total
total = 0

class ProdutoDao:
    def __init__(self, db):
        self.__db = db

    def salvar(self, produto):
        cursor = self.__db.connection.cursor()

        if (produto.id):
            cursor.execute(SQL_ATUALIZA_PRODUTO, (produto.nome, produto.valor, produto.categoria, produto.id))
        else:
            cursor.execute(SQL_CRIA_PRODUTO, (produto.nome, produto.valor, produto.categoria, True))
            produto.id = cursor.lastrowid
        self.__db.connection.commit()
        return produto

    def listar(self):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_BUSCA_PRODUTOS)
        produtos = traduz_produtos(cursor.fetchall())
        return produtos

    def listar_produtos_vendidos(self):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_BUSCA_PRODUTOS_VENDIDOS)
        produtos = traduz_produtos_vendidos(cursor.fetchall())
        return produtos        

    def busca_por_id(self, id):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_PRODUTO_POR_ID, (id,))
        tupla = cursor.fetchone()
        return Produto(tupla[1], tupla[2], tupla[3], id=tupla[0])

    def deletar(self, id):
        self.__db.connection.cursor().execute(SQL_DELETA_PRODUTO, (id, ))
        self.__db.connection.commit()


class UsuarioDao:
    def __init__(self, db):
        self.__db = db

    def buscar_por_id(self, id):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_USUARIO_POR_ID, (id,))
        dados = cursor.fetchone()
        usuario = traduz_usuario(dados) if dados else None
        return usuario

class VendaDao:
    def __init__(self, db):
        self.__db = db

    def adicionar_produto_no_carrinho(self, id):
        lista_produtos_venda.append(id)

    def retorna_lista(self):
        return lista_produtos_venda

    def retornar_produtos_do_carrinho(self):
        produtos_da_venda = []
        for produto in lista_produtos_venda:
            cursor = self.__db.connection.cursor()
            cursor.execute(SQL_PRODUTO_POR_ID, (produto,))
            tupla = cursor.fetchone()
            prod = Produto(tupla[1], tupla[2], tupla[3], id=tupla[0])
            produtos_da_venda.append(prod)
        return produtos_da_venda

    def retornar_valor_compra(self, produtos_do_carrinho):
        total = 0    
        for produto in produtos_do_carrinho:
            total = total + produto.valor    
        return total   

    def salvar_venda(self, total_venda):
        global lista_produtos_venda

        produtos_da_venda = []
        for produto in lista_produtos_venda:
            cursor = self.__db.connection.cursor()
            cursor.execute(SQL_PRODUTO_POR_ID, (produto,))
            tupla = cursor.fetchone()
            prod = Produto(tupla[1], tupla[2], tupla[3], id=tupla[0])
            produtos_da_venda.append(prod)  

        data_atual = date.today()
        venda = Venda(total_venda, data_atual)
        print(datetime.now().strftime("%H:%M:%S"))

        cursor.execute(SQL_CRIA_VENDA, (venda.valor_venda, date.today(), datetime.now().strftime("%H:%M:%S")))
        venda.id = cursor.lastrowid
        self.__db.connection.commit()

        for produto in produtos_da_venda:
            cursor = self.__db.connection.cursor()
            cursor.execute(SQL_CRIA_PRODUTOS_VENDA, (venda.id, produto.id))
            self.__db.connection.commit()
            cursor = self.__db.connection.cursor()
            cursor.execute(SQL_ATUALIZA_PRODUTO_VENDIDO, (False, produto.id))
            self.__db.connection.commit()
        lista_produtos_venda = []     


def traduz_produtos(produtos):
    def cria_produto_com_tupla(tupla):
        return Produto(tupla[1], tupla[2], tupla[3], id=tupla[0])
    return list(map(cria_produto_com_tupla, produtos))

def traduz_produtos_vendidos(produtos):
    def cria_produto_com_tupla(tupla):
        return ProdutoVenda(tupla[1], tupla[2], tupla[3], tupla[4], tupla[5], id=tupla[0])
    return list(map(cria_produto_com_tupla, produtos))    

def traduz_usuario(tupla):
    return Usuario(tupla[0], tupla[1], tupla[2])