from flask import Flask, render_template, request, redirect, session, flash, url_for
from models import Produto, Usuario, Venda
from dao import ProdutoDao, UsuarioDao, VendaDao
from flask_mysqldb import MySQL
import os

app = Flask(__name__)
app.secret_key = 'pydoca'
app.config['MYSQL_HOST'] = "fugfonv8odxxolj8.cbetxkdyhwsb.us-east-1.rds.amazonaws.com"
app.config['MYSQL_USER'] = "hbdkco2ifgta2am8"
app.config['MYSQL_PASSWORD'] = "dc11nwf7u9woe4qs"
app.config['MYSQL_DB'] = "rodrzm7397z5q8sk"
app.config['MYSQL_PORT'] = 3306

db = MySQL(app)

produto_dao = ProdutoDao(db)
usuario_dao = UsuarioDao(db)
venda_dao = VendaDao(db)

@app.route('/')
def index():
    if 'user_logged' not in session or session['user_logged'] == None:
        return redirect(url_for('login', next=url_for('index')))
    produtos = produto_dao.listar()
    lista_compra = venda_dao.retorna_lista()
    tamanho = len(lista_compra)
    return render_template('products.html', titulo='Produtos', produtos=produtos, lista_compra=lista_compra, tamanho=tamanho)

@app.route('/solds')
def solds():
    if 'user_logged' not in session or session['user_logged'] == None:
        return redirect(url_for('login', next=url_for('solds')))
    produtos = produto_dao.listar_produtos_vendidos()
    return render_template('soldproducts.html', titulo='Produtos vendidos', produtos=produtos)

@app.route('/new')
def new():
    if 'user_logged' not in session or session['user_logged'] == None:
        return redirect(url_for('login', next=url_for('new')))
    return render_template('newproduct.html', titulo='Novo Produto')

@app.route('/create', methods=['POST'])
def create():
    nome = request.form['nome']
    valor = request.form['valor']
    categoria = request.form['categoria']
    produto = Produto(nome, valor, categoria)
    produto_dao.salvar(produto)

    return redirect(url_for('index'))

@app.route('/edit/<int:id>')
def edit(id):
    if 'user_logged' not in session or session['user_logged'] == None:
        return redirect(url_for('login', next=url_for('edit')))
    produto = produto_dao.busca_por_id(id)
    return render_template('editproduct.html', titulo='Editando Produto', produto=produto)

@app.route('/update', methods=['POST'])
def update():
    nome = request.form['nome']
    valor = request.form['valor']
    categoria = request.form['categoria']
    produto = Produto(nome, valor, categoria, id=request.form['id'])
    produto_dao.salvar(produto)

    return redirect(url_for('index'))

@app.route('/delete/<int:id>')
def delete(id):
    produto_dao.deletar(id)
    flash('Produto removido com sucesso!')
    return redirect(url_for('index'))

@app.route('/adicionar/<int:id>')
def adicionar_produto_no_carrinho(id):
    venda_dao.adicionar_produto_no_carrinho(id)
    return redirect(url_for('index'))

@app.route('/carrinho')
def listar_produtos_venda():
    produtos_venda = venda_dao.retornar_produtos_do_carrinho()
    total = venda_dao.retornar_valor_compra(produtos_venda)
    return render_template('saleproducts.html', titulo='Carrinho de compras', produtos_venda=produtos_venda, total=total)

@app.route('/salvarvenda')
def salvarvenda():
    produtos_venda = venda_dao.retornar_produtos_do_carrinho()
    total = venda_dao.retornar_valor_compra(produtos_venda)
    venda_dao.salvar_venda(total)
    return redirect(url_for('index'))

@app.route('/login')
def login():
    next = request.args.get('next')
    return render_template('login.html', next=next)

@app.route('/authenticate', methods=['POST'])
def authenticate():
    usuario = usuario_dao.buscar_por_id(request.form['usuario'])
    if usuario:
        if usuario.senha == request.form['senha']:
            session['user_logged'] = usuario.id
            flash(usuario.nome + ' Logou com sucesso!')
            next_page = request.form['next']
            return redirect(next_page)
    else:
        flash('Não logado, tente novamente')
        return redirect(url_for('login'))

@app.route('/logout')
def logout():
    session['user_logged'] = None
    flash('Deslogado com sucesso!')
    return redirect(url_for('index'))

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)