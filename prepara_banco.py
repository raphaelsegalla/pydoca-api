import MySQLdb
print('Conectando...')
conn = MySQLdb.connect(user='hbdkco2ifgta2am8', passwd='dc11nwf7u9woe4qs', host='fugfonv8odxxolj8.cbetxkdyhwsb.us-east-1.rds.amazonaws.com', port=3306)

# Descomente se quiser desfazer o banco...
conn.cursor().execute("DROP DATABASE `rodrzm7397z5q8sk`;")
conn.commit()

criar_tabelas = '''SET NAMES latin1;
    CREATE DATABASE `rodrzm7397z5q8sk` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
    USE `rodrzm7397z5q8sk`;
    CREATE TABLE `produto` (
      `id` int(20) NOT NULL AUTO_INCREMENT,
      `nome` varchar(50) COLLATE utf8_bin NOT NULL,
      `valor` decimal(10,2)  NOT NULL,
      `categoria` varchar(40) COLLATE utf8_bin NOT NULL,
      `vendido` boolean NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    CREATE TABLE `usuario` (
      `id` varchar(20) COLLATE utf8_bin NOT NULL,
      `nome` varchar(20) COLLATE utf8_bin NOT NULL,
      `senha` varchar(8) COLLATE utf8_bin NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    CREATE TABLE `venda` (
      `id` int(20) NOT NULL AUTO_INCREMENT,   
      `valor_venda` decimal(10,2)  NOT NULL, 
      `data_venda` DATE NOT NULL,
      `hora_venda` TIME NOT NULL, 
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
    CREATE TABLE `produtosvenda` (
      `id_venda` int(20) NOT NULL,
      `id_produto` int(20) NOT NULL,
      FOREIGN KEY (`id_venda`) REFERENCES venda(`id`) ON DELETE CASCADE,
	FOREIGN KEY (`id_produto`) REFERENCES produto(`id`) ON DELETE CASCADE,
	UNIQUE (`id_venda`, `id_produto`)     
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;'''

conn.cursor().execute(criar_tabelas)

# inserindo usuarios
cursor = conn.cursor()
cursor.executemany(
      'INSERT INTO rodrzm7397z5q8sk.usuario (id, nome, senha) VALUES (%s, %s, %s)',
      [
            ('raphael', 'Raphael Segalla', '416424'),
            ('alessandra', 'Alessandra Liberto', 'thaina'),
            ('thaina', 'Thaina Liberato', 'ccxp')
      ])

cursor.execute('select * from rodrzm7397z5q8sk.usuario')
print(' -------------  Usuários:  -------------')
for user in cursor.fetchall():
    print(user[1])

# inserindo produtos
cursor.executemany(
      'INSERT INTO rodrzm7397z5q8sk.produto (nome, valor, categoria, vendido) VALUES (%s, %s, %s, %s)',
      [
            ('Pão de forma', '5.00', 'Panificação',True),
            ('Pão de forma', '5.00', 'Panificação',True),
            ('Pão de forma', '5.00', 'Panificação',True),
            ('Pão de forma', '5.00', 'Panificação',True),
            ('Leite B', '4.00', 'Laticínio',True),
            ('Leite B', '4.00', 'Laticínio',True),
            ('Leite B', '4.00', 'Laticínio',True),
            ('Leite B', '4.00', 'Laticínio',True),
            ('Cafe com leite', '2.50', 'Bebida',True),
            ('Cafe com leite', '2.50', 'Bebida',True),
            ('Cafe com leite', '2.50', 'Bebida',True),
            ('Cafe com leite', '2.50', 'Bebida',True),
            ('Sorvete', '3.00', 'Laticínio',True),
            ('Sorvete', '3.00', 'Laticínio',True),
            ('Sorvete', '3.00', 'Laticínio',True),
            ('Sorvete', '3.00', 'Laticínio',True),
            ('Coca-Cola lata 350ml', '4.50', 'Bebidas',True),
            ('Coca-Cola lata 350ml', '4.50', 'Bebidas',True),
            ('Coca-Cola lata 350ml', '4.50', 'Bebidas',True),
            ('Coca-Cola lata 350ml', '4.50', 'Bebidas',True),
            ('Coca-Cola lata 350ml', '4.50', 'Bebidas',True),
            ('Coca-Cola lata 350ml', '4.50', 'Bebidas',True),
            ('Coca-Cola lata 350ml', '4.50', 'Bebidas',True)
      ])

cursor.execute('select * from rodrzm7397z5q8sk.produto')
print(' -------------  Produtos:  -------------')
for produto in cursor.fetchall():
    print(produto[1])

# commitando senão nada tem efeito
conn.commit()
cursor.close()