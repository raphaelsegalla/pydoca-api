class Produto:
    def __init__(self, nome, valor, categoria, id=None):
        self.id = id
        self.nome = nome
        self.valor = valor
        self.categoria = categoria

class ProdutoVenda:
    def __init__(self, nome, valor, categoria, data_venda, hora_venda, id=None):
        self.id = id
        self.nome = nome
        self.valor = valor
        self.categoria = categoria
        self.data_venda = data_venda    
        self.hora_venda = hora_venda    

class Usuario:
    def __init__(self, id, nome, senha):
        self.id = id
        self.nome = nome
        self.senha = senha

class Venda:
    def __init__(self, valor_venda, data_venda, id=None):
        self.id = id
        self.valor_venda = valor_venda
        self.data_venda = data_venda
             